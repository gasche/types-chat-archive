This repossitory hosts the [public archives](https://gasche.gitlab.io/types-chat-archive/) of the [Types zulip chat](https://typ.zulipchat.com/):

- Types Zulip chat: https://typ.zulipchat.com/
- public archives: https://gasche.gitlab.io/types-chat-archive/

It is an instance of the generic script
[zulip-archive-gitlab-ci](https://gitlab.com/gasche/zulip-archive-gitlab-ci);
go there for more information on how to easily setup your own archive
of your own Zulip chat, on top of Gitlab CI and Gitlab Pages.
